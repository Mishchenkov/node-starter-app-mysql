//now it load express module with `require` directive
var port = process.env.PORT_NUMBER;
var dburl = process.env.DATABASE_URL;
var hostname = process.env.HOSTNAME;
var express = require('express')
var app = express()

var pgp = require("pg-promise")(/*options*/);
var db = pgp(dburl);

db.none('INSERT INTO login (host, last_login) VALUES ($1, NOW())', hostname)
    .catch(function(err) {
        console.log('Error on insert into my-table: ' + err);
    });

app.get('/login', function(req, res, next) {
        db.any("SELECT * FROM login LIMIt 1000")
                .then(function (data) {
                        console.log("DATA:", data);
                        res.json(data)
                })
                .catch(function (error) {
                        console.log("ERROR:", error);
                });
});

//Define request response in root URL (/) and response with text Hello World!
app.get('/', function (req, res) {
 res.send(`Hello World! Port: ${port}`)
})
//Launch listening server on port PORT_NUMBER and consoles the log.
app.listen(port, function () {
 console.log('app listening on port %d!', port)
})
