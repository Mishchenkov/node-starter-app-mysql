FROM mhart/alpine-node:10

USER root

WORKDIR /tmp

#ADD utils
RUN apk --update add postgresql-client

# Create directories

RUN mkdir -p /app/www/


# Set permissions

RUN chmod -R 0755 /app
RUN adduser -D -S -u 333 -g "" "www-data"
RUN chown -R www-data /app


# Clean

RUN rm -rf /tmp/*
RUN rm -rf /var/cache/apk/*


# Build app

USER www-data
WORKDIR /app/www
COPY --chown=www-data:nogroup . /app/www/
RUN mkdir -p /app/www/log && \
    chown www-data:nogroup /app/www/log

RUN npm install


# Well done

USER root
WORKDIR /app/www
EXPOSE 8080 
CMD node /app/www/
#CMD . /app/www/env.sh && nodemon /app/www/
